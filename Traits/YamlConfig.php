<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Traits;

use OxidEsales\Eshop\Core\Registry;
use Symfony\Component\Yaml\Yaml;
use TheRealWorld\ToolsPlugin\Core\ToolsYaml;

/**
 * trait YamlConfig.
 */
trait YamlConfig
{
    use Definitions;

    protected string $sYamlFileMask = '%s%s%s.yaml';
    protected string $sYamlConfigPath = DIRECTORY_SEPARATOR . 'var' .
        DIRECTORY_SEPARATOR . 'trwclirun' .
        DIRECTORY_SEPARATOR;
    protected string $sYamlExportImportPath = DIRECTORY_SEPARATOR . 'var' .
        DIRECTORY_SEPARATOR . 'trwcliexportimport' .
        DIRECTORY_SEPARATOR;

    /**
     * Set Yaml Config.
     */
    public function setYamlConfig(string $sYamlFile, array $aData): void
    {
        ToolsYaml::setDataToYamlFile(
            $sYamlFile,
            $aData
        );
    }

    protected function getYamlConfigFile(?int $iShopId = null, string $sEnvironment = ''): string
    {
        return sprintf(
            $this->sYamlFileMask,
            $this->getYamlConfigPath(),
            $sEnvironment ? $sEnvironment . '.' : '',
            $iShopId ?? Registry::getConfig()->getShopId()
        );
    }

    protected function getYamlEnvironmentFile(?int $iShopId = null, string $sEnvironment = ''): string
    {
        return sprintf(
            $this->sYamlFileMask,
            $this->getYamlEnvironmentPath(),
            $sEnvironment ? $sEnvironment . '.' : '',
            $iShopId ?? Registry::getConfig()->getShopId()
        );
    }

    protected function getConfigurationFile(?int $iShopId = null): string
    {
        return sprintf(
            $this->sYamlFileMask,
            $this->getConfigurationPath(),
            '',
            $iShopId ?? Registry::getConfig()->getShopId()
        );
    }

    protected function getConfigurationTransferFile(?int $iShopId = null, string $sEnvironment = ''): string
    {
        return sprintf(
            $this->sYamlFileMask,
            $this->getConfigurationTransferPath($iShopId, $sEnvironment),
            $sEnvironment ? $sEnvironment . '.' : '',
            $iShopId ?? Registry::getConfig()->getShopId()
        );
    }

    protected function getYamlConfigPath(): string
    {
        return $this->getRealPath(
            $this->sYamlConfigPath,
            false
        );
    }

    protected function getYamlExportImportPath(): string
    {
        return $this->getRealPath(
            $this->sYamlExportImportPath,
            false
        );
    }

    protected function getYamlEnvironmentPath(): string
    {
        return $this->getRealPath('var' .
            DIRECTORY_SEPARATOR . 'configuration' .
            DIRECTORY_SEPARATOR . 'environment' .
            DIRECTORY_SEPARATOR, false);
    }

    protected function getConfigurationPath(): string
    {
        return $this->getRealPath('var' .
            DIRECTORY_SEPARATOR . 'configuration' .
            DIRECTORY_SEPARATOR . 'shops' .
            DIRECTORY_SEPARATOR, false);
    }

    protected function getConfigurationTransferPath(?int $iShopId = null, string $sEnvironment = ''): string
    {
        $aCliRunConfig = $this->getYamlConfig(
            $iShopId,
            $sEnvironment
        );

        $sTransferPath = $aCliRunConfig['configExportImportPath'] ?? 'transfer';

        return $this->getRealPath('var' .
            DIRECTORY_SEPARATOR . 'configuration' .
            DIRECTORY_SEPARATOR . $sTransferPath .
            DIRECTORY_SEPARATOR, false);
    }

    protected function getYamlConfig(?int $iShopId = null, string $sEnvironment = ''): ?array
    {
        $iShopId ??= Registry::getConfig()->getShopId();

        $aCliRunConfig = null;

        $sYamlFile = $this->getYamlConfigFile($iShopId, $sEnvironment);

        if ($sYamlFile) {
            // now try to read YAML
            $oYaml = ToolsYaml::getYamlStringFromFile($sYamlFile);
            $aCliRunConfig = Yaml::parse($oYaml);
        } else {
            $this->output->writeLn(sprintf(
                '<comment>No clirun-config for shop-id `%s` and Environment `%s` found. Take default.</comment>',
                $iShopId,
                $sEnvironment
            ));
        }

        if ($sYamlFile && !is_array($aCliRunConfig)) {
            $this->output->writeLn(sprintf(
                '<comment>No valid YAML data found: `%s`</comment>',
                realpath($sYamlFile)
            ));

            return [];
        }

        return $aCliRunConfig;
    }

    /**
     * @SuppressWarnings(PHPMD.StaticAccess)
     * @SuppressWarnings(PHPMD.ElseExpression)
     */
    protected function getYamlExportImportConfig(string $yamlFileName = ''): array
    {
        $cliRunConfig = [];

        $yamlFile = $this->getYamlExportImportFile($yamlFileName);
        if ($yamlFile) {
            // now try to read YAML
            $this->output->writeLn(sprintf(
                '<comment>Use config: `%s`</comment>',
                realpath($yamlFile)
            ));
            $yaml = ToolsYaml::getYamlStringFromFile($yamlFile);
            $cliRunConfig = ToolsYaml::getYamlContent($yaml);
        } else {
            $this->output->writeLn(
                '<comment>No configuration passed, default used.</comment>',
            );
            $this->writeDefaultExportImportYaml();
        }

        if ($yamlFile && !count($cliRunConfig)) {
            $cliRunConfig = [];
            $this->output->writeLn(sprintf(
                '<comment>No valid YAML data found: `%s`</comment>',
                realpath($yamlFile)
            ));
        }

        // bulletproof config
        $defaultConfig = $this->getExportImportDefault();
        $cliRunConfig[$this->confKeyDump] =
            is_string($cliRunConfig[$this->confKeyDump])
            && !empty($cliRunConfig[$this->confKeyDump]) ?
                $cliRunConfig[$this->confKeyDump] :
                $defaultConfig[$this->confKeyDump];
        $cliRunConfig[$this->confKeyTable] = is_array($cliRunConfig[$this->confKeyTable]) ?
            $cliRunConfig[$this->confKeyTable] :
            [];
        $cliRunConfig[$this->confKeyAnonymize] = is_array($cliRunConfig[$this->confKeyAnonymize]) ?
            $cliRunConfig[$this->confKeyAnonymize] :
            [];

        return $cliRunConfig;
    }

    protected function getYamlExportImportFile(string $yamlFileName = ''): string
    {
        $yamlFile = $this->getYamlExportImportPath() . $yamlFileName;

        return ($yamlFileName && file_exists($yamlFile)) ? $yamlFile : '';
    }

    protected function getExportImportDefault(): array
    {
        return [
            $this->confKeyDump      => 'dump.sql',
            $this->confKeyTable     => [],
            $this->confKeyAnonymize => [
                'oxuser' => [
                    'oxfname', 'oxlname',
                ],
                'oxorder' => [
                    'oxbillfname', 'oxbilllname', 'oxdelfname', 'oxdellname',
                ],
            ],
        ];
    }

    protected function writeDefaultExportImportYaml(): string
    {
        $yamlFile = 'example.yaml';
        if (!$this->getYamlExportImportFile($yamlFile)) {
            $yamlFilePath = $this->getYamlExportImportPath() . $yamlFile;
            ToolsYaml::setDataToYamlFile(
                $yamlFilePath,
                $this->getExportImportDefault()
            );
            $this->output->writeLn(sprintf(
                '<comment>Create a %s as example. You can use this file with parameter --yaml=%s</comment>',
                realpath($yamlFilePath),
                $yamlFile
            ));
        }

        return $yamlFile;
    }

    /**
     * return the YamlConfig.
     *
     * @param null|int    $iShopId   - ShopId
     * @param null|string $sYamlFile - possible Config-File
     */
    protected function getShopConfiguration(
        array $aExcludeConfigFields = [],
        ?int $iShopId = null,
        ?string $sYamlFile = null
    ): ?array {
        $iShopId ??= Registry::getConfig()->getShopId();

        $aYamlValues = null;

        // Fallback, read the original ShopConfiguration
        $sYamlFile ??= $this->getConfigurationFile($iShopId);

        if ($sYamlFile) {
            // now try to read YAML
            $oYaml = ToolsYaml::getYamlStringFromFile($sYamlFile);
            $aYamlValues = Yaml::parse($oYaml);

            // remove all not allowed Vars
            if (count($aExcludeConfigFields)) {
                // Module-Options
                foreach ($aYamlValues['modules'] as $sModuleId => $aModuleValues) {
                    // !important at least one empty array must be present so that
                    // there are no difficulties with the import.
                    if (!isset($aModuleValues['moduleSettings'])) {
                        $aModuleValues['moduleSettings'] = [];
                    }
                    foreach ($aModuleValues['moduleSettings'] as $sModuleVar => $aModuleValue) {
                        if (in_array($sModuleVar, $aExcludeConfigFields, true)) {
                            unset($aYamlValues['modules'][$sModuleId]['moduleSettings'][$sModuleVar]);
                        }
                    }
                }
                // Theme-Options
                foreach ($aYamlValues['themes'] as $sThemeId => $aThemeValues) {
                    foreach ($aThemeValues['themeSettings'] as $sThemeVar => $aThemeValue) {
                        if (in_array($sThemeVar, $aExcludeConfigFields, true)) {
                            unset($aYamlValues['themes'][$sThemeId]['themeSettings'][$sThemeVar]);
                        }
                    }
                }
                // ShopConfig-Options
                foreach ($aYamlValues['shopconfig'] as $sConfigVar => $aConfigValue) {
                    if (in_array($sConfigVar, $aExcludeConfigFields, true)) {
                        unset($aYamlValues['shopconfig'][$sConfigVar]);
                    }
                }
                // ShopTableConfig-Options
                foreach ($aYamlValues['shoptable'] as $sConfigVar => $aConfigValue) {
                    if (in_array($sConfigVar, $aExcludeConfigFields, true)) {
                        unset($aYamlValues['shoptable'][$sConfigVar]);
                    }
                }
            }
        }

        if (!$aYamlValues || !is_array($aYamlValues)) {
            $this->output->writeLn('<comment>No valid YAML data found: `' . realpath($sYamlFile) . '`</comment>');

            return null;
        }

        return $aYamlValues;
    }
}
