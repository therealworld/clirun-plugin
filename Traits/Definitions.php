<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Traits;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * trait Definitions.
 */
trait Definitions
{
    protected string $confKeyDump = 'dumpFileName';
    protected string $confKeyTable = 'onlyTables';
    protected string $confKeyAnonymize = 'anonymizeRowsInTables';

    /**
     * OutputInterface instance.
     */
    protected OutputInterface $output;

    /**
     * InputInterface instance.
     */
    protected InputInterface $input;
}
