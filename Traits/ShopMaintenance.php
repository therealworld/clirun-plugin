<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Traits;

use Exception;
use JsonException;
use OxidEsales\Eshop\Application\Model\Shop;
use OxidEsales\Eshop\Core\Registry;

/**
 * trait ShopMaintenance.
 */
trait ShopMaintenance
{
    /**
     * ignored files in image folders.
     *
     * @var array;
     */
    private array $aIgnoredFiles = [
        'nopics.jpg', 'dir.txt',
    ];

    /**
     * ignored folders in image folders.
     *
     * @var array;
     */
    private array $aIgnoredFolders = [
        '.', '..',
    ];

    /**
     * is Update possible.
     *
     * @param array    $aCliRunConfig an Array with Yaml Options
     * @param null|int $iShopId       iShopId
     *
     * @return mixed (boolean: true or String: Notice why not)
     *
     * @throws JsonException
     */
    private function isUpdatePossible(array $aCliRunConfig = [], ?int $iShopId = null)
    {
        $iShopId ??= Registry::getConfig()->getShopId();

        $aDontRunIfFileExits = $aCliRunConfig['dontRunIfFileExits'] ?? [];
        $aRunIfFileExits = $aCliRunConfig['runIfFileExits'] ?? [];
        $aDontRunIfOptionValue = $aCliRunConfig['dontRunIfOptionValue'] ?? [];
        $aRunIfOptionValue = $aCliRunConfig['runIfOptionValue'] ?? [];
        $aRunIfEnvironmentVariable = $aCliRunConfig['runIfEnvironmentVariable'] ?? [];

        if (
            (false === (
                $mResult = $this->isFileExistsDontRun(
                    $aDontRunIfFileExits,
                    $iShopId
                )
            ))
            && (true === (
                $mResult = $this->isFileExistsRun(
                    $aRunIfFileExits,
                    $iShopId
                )
            ))
            && (false === (
                $mResult = $this->isOptionValueDontRun(
                    $aDontRunIfOptionValue,
                    $iShopId
                )
            ))
            && (true === (
                $mResult = $this->isOptionValueRun(
                    $aRunIfOptionValue,
                    $iShopId
                )
            ))
            && true === ($mResult = $this->isEnvironmentVariableRun(
                $aRunIfEnvironmentVariable,
                $iShopId
            ))
        ) {
            $mResult = true;
        }

        return $mResult;
    }

    /**
     * return string from variable.
     *
     * @param mixed $mVar
     *
     * @throws JsonException
     */
    private function convertToString($mVar): string
    {
        return (is_array($mVar) || is_object($mVar)) ? json_encode($mVar, JSON_THROW_ON_ERROR) : (string) $mVar;
    }

    /**
     * is File exists: dont run.
     *
     * @param array $aCheckFiles an Array with Filenames
     *
     * @return bool|string (boolean: false or String: Notice why not)
     */
    private function isFileExistsDontRun(array $aCheckFiles = [], ?int $iShopId = null)
    {
        $mResult = false;

        if (is_array($aCheckFiles)) {
            foreach ($aCheckFiles as $sCheckFile) {
                if (file_exists($sCheckFile)) {
                    $mResult = sprintf(
                        "IMPORTANT NOTE: The Installation for Shop-ID '%s' was cancelled. " .
                            "The file '%s' exists.",
                        $iShopId,
                        $sCheckFile
                    );

                    break;
                }
            }
        }

        return $mResult;
    }

    /**
     * is File exists: run.
     *
     * @param array    $aCheckFiles an Array with Filenames
     * @param null|int $iShopId     iShopId
     *
     * @return string|true (boolean: true or String: Notice why not)
     */
    private function isFileExistsRun(array $aCheckFiles = [], ?int $iShopId = null)
    {
        $mResult = true;

        if (is_array($aCheckFiles)) {
            foreach ($aCheckFiles as $sCheckFile) {
                if (!file_exists($sCheckFile)) {
                    $mResult = sprintf(
                        "IMPORTANT NOTE: The Installation for Shop-ID '%s' was cancelled. " .
                            "The file '%s' not exists.",
                        $iShopId,
                        $sCheckFile
                    );

                    break;
                }
            }
        }

        return $mResult;
    }

    /**
     * is Option Value true: dont run.
     *
     * @param array    $aCheckValues an Array with Values
     * @param null|int $iShopId      iShopId
     *
     * @return false|string (boolean: false or String: Notice why not)
     *
     * @throws JsonException
     */
    private function isOptionValueDontRun(array $aCheckValues = [], ?int $iShopId = null)
    {
        $mResult = false;

        if (is_array($aCheckValues)) {
            foreach ($aCheckValues as $sCheckOption => $mShouldOption) {
                $mCheckOption = Registry::getConfig()->getConfigParam($sCheckOption);
                if ($mCheckOption === $mShouldOption) {
                    $mResult = sprintf(
                        "IMPORTANT NOTE: The Installation for Shop-ID '%s' was cancelled. " .
                            "The option '%s' HAS the value '%s'.",
                        $iShopId,
                        $sCheckOption,
                        $this->convertToString($mShouldOption)
                    );

                    break;
                }
            }
        }

        return $mResult;
    }

    /**
     * is Option Value true: run.
     *
     * @param array    $aCheckValues an Array with Values
     * @param null|int $iShopId      iShopId
     *
     * @return string|true (boolean: true or String: Notice why not)
     *
     * @throws JsonException
     */
    private function isOptionValueRun(array $aCheckValues = [], ?int $iShopId = null)
    {
        $mResult = true;

        if (is_array($aCheckValues)) {
            foreach ($aCheckValues as $sCheckOption => $mShouldOption) {
                $mCheckOption = Registry::getConfig()->getConfigParam($sCheckOption);
                if ($mCheckOption !== $mShouldOption) {
                    $mResult = sprintf(
                        "IMPORTANT NOTE: The Installation for Shop-ID '%s' was cancelled. " .
                            "The option '%s' HAS NOT the value '%s'.",
                        $iShopId,
                        $sCheckOption,
                        $this->convertToString($mShouldOption)
                    );

                    break;
                }
            }
        }

        return $mResult;
    }

    /**
     * is Environment Variables true: run.
     *
     * @param array    $aCheckValues an Array with Values
     * @param null|int $iShopId      iShopId
     *
     * @return string|true (boolean: true or String: Notice why not)
     *
     * @throws JsonException
     */
    private function isEnvironmentVariableRun(array $aCheckValues = [], ?int $iShopId = null)
    {
        $mResult = true;

        if (is_array($aCheckValues)) {
            foreach ($aCheckValues as $sCheckOption => $mShouldOption) {
                $mCheckOption = getenv($sCheckOption);
                if ($mCheckOption !== $mShouldOption) {
                    $mResult = sprintf(
                        "IMPORTANT NOTE: The Installation for Shop-ID '%s' was cancelled. " .
                            "The ENVIRONMENT-Variable '%s' DOES NOT EXISTS or HAS NOT the value '%s'.",
                        $iShopId,
                        $sCheckOption,
                        $this->convertToString($mShouldOption)
                    );

                    break;
                }
            }
        }

        return $mResult;
    }

    /**
     * set the Shop Status ON/OFF.
     *
     * @param int $iStatus status (0,1)
     *
     * @return void
     *
     * @throws Exception
     */
    private function setShopStatus(int $iStatus = 1, ?int $iShopId = null): bool
    {
        if (is_null($iShopId)) {
            $oShop = Registry::getConfig()->getActiveShop();
        } else {
            $oShop = oxNew(Shop::class);
            $oShop->load($iShopId);
        }

        $aParams = [
            'oxshops__oxactive' => $iStatus,
        ];

        $oShop->assign($aParams);
        $oShop->save();

        return true;
    }

    /**
     * set the Shop Maintenance Mode ON.
     *
     * @throws Exception
     */
    private function setShopMaintenanceOn(?int $iShopId = null): bool
    {
        // Maintenance Mode ON means that the shop is OFF
        return $this->setShopStatus(0, $iShopId);
    }

    /**
     * set the Shop Maintenance Mode OFF.
     *
     * @throws Exception
     */
    private function setShopMaintenanceOff(?int $iShopId = null): bool
    {
        // Maintenance Mode OFF means that the shop is ON
        return $this->setShopStatus(1, $iShopId);
    }

    /**
     * delete a folder recursivly.
     */
    private function rrmdir(string $sPath): bool
    {
        $bResult = false;

        if (is_dir($sPath)) {
            $aList = scandir($sPath);
            foreach ($aList as $sFile) {
                if (!in_array($sFile, $this->aIgnoredFolders, true)) {
                    if (is_dir($sPath . DIRECTORY_SEPARATOR . $sFile)) {
                        $this->rrmdir($sPath . DIRECTORY_SEPARATOR . $sFile);
                    } else {
                        unlink($sPath . DIRECTORY_SEPARATOR . $sFile);
                    }
                }
            }
            rmdir($sPath);
            $bResult = true;
        }

        return $bResult;
    }

    /**
     * get the content of a dir.
     *
     * @param mixed $aResults
     */
    private function getDirContents(string $sDir, &$aResults = [])
    {
        $aFiles = scandir($sDir);

        foreach ($aFiles as $sFile) {
            $sPath = realpath($sDir . DIRECTORY_SEPARATOR . $sFile);
            if (!is_dir($sPath) && !in_array($sFile, $this->aIgnoredFiles, true)) {
                $aResults[] = $sPath;
            } elseif (is_dir($sPath) && !in_array($sFile, $this->aIgnoredFolders, true)) {
                $this->getDirContents($sPath, $aResults);
            }
        }

        return $aResults;
    }
}
