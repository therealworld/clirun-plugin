<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Traits;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Facts\Facts;

/**
 * trait CommandLine.
 */
trait CommandLine
{
    use Definitions;

    protected function getShopIds(): array
    {
        $oConfig = Registry::getConfig();
        $iShopId = $this->getOptionShopId();

        $aShopIds = [];
        if ($iShopId && !$this->isMultiShop()) {
            $aShopIds[] = $iShopId;
        }
        if ($this->isMultiShop()) {
            $aShopIds = $oConfig->getShopIds();
        }

        return $aShopIds;
    }

    protected function isMultiShop(): bool
    {
        return !(Registry::getConfig()->getEdition() === 'CE');
    }

    protected function getOptionShopId(): ?int
    {
        $iShopId = null;
        if ($this->input->hasOption('shop-id')) {
            $iShopId = $this->input->getOption('shop-id');
            $iShopId = is_numeric($iShopId) ? (int) $iShopId : null;
        }
        if (!$iShopId) {
            $iShopId = Registry::getConfig()->getShopId();
        }

        return $iShopId;
    }

    protected function getOptionDBYaml(): string
    {
        $result = '';
        if ($this->input->hasOption('yaml')) {
            $result = $this->input->getOption('yaml');
            $result = is_string($result) ? $result : '';
        }

        return $result;
    }

    protected function getOptionEnvironment(): string
    {
        $sEnvironment = '';
        if ($this->input->hasOption('env')) {
            $sEnvironment = $this->input->getOption('env');
            $sEnvironment = is_string($sEnvironment) ? $sEnvironment : '';
        }

        return $sEnvironment;
    }

    protected function getRealPath(string $path, bool $sourcePath): string
    {
        $facts = new Facts();
        $root = $sourcePath ? $facts->getSourcePath() : $facts->getShopRootPath();
        $root = rtrim($root, DIRECTORY_SEPARATOR);
        $base = realpath($root) . DIRECTORY_SEPARATOR . $path;
        $this->checkPath($base);

        return $base;
    }

    protected function checkPath(string $path): string
    {
        if (
            !file_exists($path)
            && !mkdir($path, 0755, true)
            && !is_dir($path)
        ) {
            $this->output->writeLn(sprintf('Directory "%s" was not created', $path));
        }

        return $path;
    }
}
