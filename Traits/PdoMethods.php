<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

/**
 * Copyright © OXID eSales AG. All rights reserved.
 * See LICENSE file for license details.
 */

namespace TheRealWorld\CliRunPlugin\Traits;

/**
 * trait PdoMethods.
 */
trait PdoMethods
{
    protected function getPdoDsnConnection(string $host, string $dbName, string $port = ''): string
    {
        return 'mysql:host=' . $host
            . ($port ? ';port=' . $port : '')
            . ';dbname=' . $dbName;
    }
}
