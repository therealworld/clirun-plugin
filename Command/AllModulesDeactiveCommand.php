<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Command;

use Exception;
use OxidEsales\EshopCommunity\Internal\Framework\Module\Setup\Exception\ModuleSetupException;
use OxidEsales\EshopCommunity\Internal\Framework\Module\Setup\Service\ModuleActivationServiceInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TheRealWorld\CliRunPlugin\Traits\CommandLine;
use TheRealWorld\CliRunPlugin\Traits\ShopMaintenance;
use TheRealWorld\CliRunPlugin\Traits\YamlConfig;

/**
 * Class AllModulesDeactiveCommand.
 */
class AllModulesDeactiveCommand extends Command
{
    use CommandLine;
    use ShopMaintenance;
    use YamlConfig;

    protected static $defaultName = 'trw:allmodules:deactive';

    private ModuleActivationServiceInterface $moduleActivationService;

    public function __construct(
        ModuleActivationServiceInterface $moduleActivationService
    ) {
        parent::__construct(null);

        $this->moduleActivationService = $moduleActivationService;
    }

    protected function configure(): void
    {
        $this->setDescription('Set all Modules and the Shop "deactive"')
            ->addOption(
                '--env',
                '',
                InputOption::VALUE_OPTIONAL,
                'Environment to execute in'
            )->addOption(
                '--shop-id',
                '',
                InputOption::VALUE_OPTIONAL,
                'possible Shop-ID'
            )
        ;
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $aShopIds = $this->getShopIds();

        foreach ($aShopIds as $iShopId) {
            if (
                !$aCliRunConfig = $this->getYamlConfig(
                    $iShopId,
                    $this->getOptionEnvironment()
                )
            ) {
                continue;
            }

            // set ShopMaintenance On
            if ($this->setShopMaintenanceOn($iShopId)) {
                $this->output->writeLn(sprintf('<comment>Maintenance Mode for Shop %s activated!</comment>', $iShopId));
            }

            // disable active Modules in a certain order
            if (isset($aCliRunConfig['moduleTakeCare'])) {
                $this->deactivateModules($aCliRunConfig['moduleTakeCare'], $iShopId);
            }

            // disable active DevelopModules in a certain order
            if (isset($aCliRunConfig['moduleTakeCareDevelop'])) {
                $this->deactivateModules($aCliRunConfig['moduleTakeCareDevelop'], $iShopId);
            }
        }

        return 0;
    }

    /**
     * disable Active Modules.
     *
     * @param array    $aModuleIds with ModuleIds
     * @param null|int $iShopId    ShopId
     */
    protected function deactivateModules(array $aModuleIds = [], ?int $iShopId = null): void
    {
        if (is_array($aModuleIds)) {
            // reverse Module certain order
            $aModuleIds = array_reverse($aModuleIds);
            foreach ($aModuleIds as $sModuleId) {
                // deactivate module
                try {
                    $this->moduleActivationService->deactivate($sModuleId, $iShopId);
                    $this->output->writeLn(
                        '<info>' . sprintf(
                            'Module - "%s" has been deactivated for Shop "%s".',
                            $sModuleId,
                            $iShopId
                        ) . '</info>'
                    );
                } catch (ModuleSetupException $exception) {
                    $this->output->writeLn(
                        '<info>' . sprintf(
                            'It was not possible to deactivate module - "%s" for Shop "%s", maybe it was not active?',
                            $sModuleId,
                            $iShopId
                        ) . '</info>'
                    );
                }
            }
        }
    }
}
