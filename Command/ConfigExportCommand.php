<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Command;

use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Exception\DatabaseException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TheRealWorld\CliRunPlugin\Traits\CommandLine;
use TheRealWorld\CliRunPlugin\Traits\YamlConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsTheme;
use TheRealWorld\ToolsPlugin\Core\ToolsYaml;

class ConfigExportCommand extends Command
{
    use CommandLine;
    use YamlConfig;

    protected static $defaultName = 'trw:config:export';

    public function configure(): void
    {
        $this
            ->setDescription('Export shop config')
            ->addOption(
                '--env',
                '',
                InputOption::VALUE_OPTIONAL,
                'Environment to execute in'
            )->addOption(
                '--shop-id',
                '',
                InputOption::VALUE_OPTIONAL,
                'possible Shop-ID'
            )
        ;
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     *
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     * @throws DatabaseException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $aShopIds = $this->getShopIds();

        foreach ($aShopIds as $iShopId) {
            if (!$aCliRunConfig = $this->getYamlConfig($iShopId)) {
                continue;
            }
            $aNotAllowedVars = $aCliRunConfig['configExportNotAllowedVars'] ?? [];

            // ModuleConfigs
            $aConfigValues = $this->getShopConfiguration($aNotAllowedVars, $iShopId);
            // ThemeConfigs
            $aConfigValues['themes'] = ToolsTheme::getAllThemeOptions($aNotAllowedVars, $iShopId);
            // ShopConfigs
            $aConfigValues['shopconfig'] = ToolsConfig::getShopConfigValuesForExport($aNotAllowedVars, $iShopId);
            // ShopTable
            $aConfigValues['shoptable'] = ToolsConfig::getShopTableValuesForExport($aNotAllowedVars, $iShopId);

            $sConfigTransferFile = $this->getConfigurationTransferFile(
                $iShopId,
                $this->getOptionEnvironment()
            );

            ToolsYaml::setDataToYamlFile($sConfigTransferFile, $aConfigValues);
            $this->output->writeln(sprintf(
                '<comment>Config exported: `%s`</comment>',
                realpath($sConfigTransferFile)
            ));
        }

        return 0;
    }
}
