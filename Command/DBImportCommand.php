<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Command;

use OxidEsales\Eshop\Core\Registry;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TheRealWorld\CliRunPlugin\Core\Import;
use TheRealWorld\CliRunPlugin\Traits\CommandLine;
use TheRealWorld\CliRunPlugin\Traits\YamlConfig;

/**
 * Class DBImportCommand.
 */
class DBImportCommand extends Command
{
    use YamlConfig;
    use CommandLine;

    protected static $defaultName = 'trw:db:import';

    /**
     * Configure Command.
     */
    protected function configure(): void
    {
        $this->setDescription(
            'Import Database from /import folder.
             Optional control via yaml-file in ' . $this->sYamlExportImportPath
        )->addOption(
            '--yaml',
            '',
            InputOption::VALUE_OPTIONAL,
            'Name of yaml-file in in config-folder ' . $this->sYamlExportImportPath
        );
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $cliRunConfig = $this->getYamlExportImportConfig(
            $this->getOptionDBYaml()
        );

        $config = Registry::getConfig();

        $this->import(
            $config->getConfigParam('dbHost'),
            $config->getConfigParam('dbName'),
            $config->getConfigParam('dbUser'),
            $config->getConfigParam('dbPwd'),
            $this->getImportPath() . $cliRunConfig[$this->confKeyDump],
            (string) $config->getConfigParam('dbPort')
        );

        return 0;
    }

    protected function import(
        string $host,
        string $dbName,
        string $userName,
        string $passWd,
        string $dumpFile,
        string $port = ''
    ): void {
        try {
            new Import(
                $dumpFile,
                $userName,
                $passWd,
                $dbName,
                $host,
                $port
            );

            $this->output->writeLn(sprintf(
                '<comment>Import completed from %s</comment>',
                $dumpFile
            ));
        } catch (RuntimeException $e) {
            $this->output->writeLn(sprintf(
                '<comment>mysql-import-php error: `%s`</comment>',
                $e->getMessage()
            ));
        }
    }

    protected function getImportPath(): string
    {
        return $this->getRealPath('import' . DIRECTORY_SEPARATOR, true);
    }
}
