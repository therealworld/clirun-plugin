<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Command;

use Exception;
use JsonException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TheRealWorld\CliRunPlugin\Traits\CommandLine;
use TheRealWorld\CliRunPlugin\Traits\ShopMaintenance;
use TheRealWorld\CliRunPlugin\Traits\YamlConfig;

/**
 * Class UpdateAfterCommand.
 */
class UpdateAfterCommand extends Command
{
    use CommandLine;
    use ShopMaintenance;
    use YamlConfig;

    protected static $defaultName = 'trw:update:after';

    protected function configure(): void
    {
        $this->setDescription('Continue the Shop after Update')
            ->addOption(
                'env',
                '',
                InputOption::VALUE_OPTIONAL,
                'Environment to execute in'
            )->addOption(
                '--shop-id',
                '',
                InputOption::VALUE_OPTIONAL,
                'possible Shop-ID'
            )
        ;
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     *
     * @throws ExceptionInterface|JsonException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $app = $this->getApplication();

        $aShopIds = $this->getShopIds();

        // delete Demo Images
        $arguments = [
            'command'          => 'trw:clear:demoimages',
            '--no-interaction' => true,
        ];
        $commandInput = new ArrayInput($arguments);
        // set Input-Object explizit to "non-interactive"
        $commandInput->setInteractive(false);
        $app->find('trw:clear:demoimages')->run($commandInput, $this->output);

        // Cache Clear
        $arguments = [
            'command' => 'trw:clear:cache',
        ];
        $commandInput = new ArrayInput($arguments);
        $app->find('trw:clear:cache')->run($commandInput, $this->output);

        foreach ($aShopIds as $iShopId) {
            if (
                !$aCliRunConfig = $this->getYamlConfig(
                    $iShopId,
                    $this->getOptionEnvironment()
                )
            ) {
                continue;
            }

            if (true !== ($mResult = $this->isUpdatePossible($aCliRunConfig, $iShopId))) {
                $this->output->writeLn($mResult);

                continue;
            }

            // activate Modules in a certain order
            $arguments = [
                'command'   => 'trw:allmodules:active',
                '--env'     => $this->getOptionEnvironment(),
                '--shop-id' => $iShopId,
            ];
            $commandInput = new ArrayInput($arguments);
            $app->find('trw:allmodules:active')->run($commandInput, $this->output);
        }

        // Move Setup-Folder if exists
        $this->renameShopSetup();

        // TplBlock Clear
        $arguments = [
            'command' => 'trw:clear:tplblock',
        ];
        $commandInput = new ArrayInput($arguments);
        $app->find('trw:clear:tplblock')->run($commandInput, $this->output);

        // Update Views
        $arguments = [
            'command' => 'trw:update:views',
        ];
        $commandInput = new ArrayInput($arguments);
        $app->find('trw:update:views')->run($commandInput, $this->output);

        return 0;
    }

    /**
     * rename the Shop Setup-Path, so that the Shop dont stop working.
     */
    protected function renameShopSetup(): void
    {
        $base = $this->getRealPath(DIRECTORY_SEPARATOR . 'source' . DIRECTORY_SEPARATOR, false);

        if (
            is_dir($base . 'Setup')
        ) {
            try {
                rename($base . 'Setup', $base . '_Setup');

                $this->output->writeLn(
                    '<comment>OXID-Shop-Setup-Path renamed to _Setup</comment>'
                );
            } catch (Exception $e) {
                $this->output->writeLn(
                    '<comment>OXID-Shop-Setup-Path could not renamed to _Setup</comment>'
                );
            }
        }
    }
}
