<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Command;

use Exception;
use OxidEsales\Eshop\Application\Model\Shop;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\EshopCommunity\Internal\Container\ContainerFactory;
use OxidEsales\EshopCommunity\Internal\Framework\Module\Configuration\Bridge\ShopConfigurationDaoBridgeInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TheRealWorld\CliRunPlugin\Traits\CommandLine;
use TheRealWorld\CliRunPlugin\Traits\YamlConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

class ConfigImportCommand extends Command
{
    use CommandLine;
    use YamlConfig;

    protected static $defaultName = 'trw:config:import';

    public function configure(): void
    {
        $this
            ->setDescription('Import shop config')
            ->addOption(
                '--env',
                '',
                InputOption::VALUE_OPTIONAL,
                'Environment to execute in'
            )->addOption(
                '--shop-id',
                '',
                InputOption::VALUE_OPTIONAL,
                'possible Shop-ID'
            )
        ;
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     *
     * @throws ContainerExceptionInterface
     * @throws DatabaseConnectionException
     * @throws ExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $app = $this->getApplication();

        $aShopIds = $this->getShopIds();

        foreach ($aShopIds as $iShopId) {
            if (!$aCliRunConfig = $this->getYamlConfig($iShopId)) {
                continue;
            }
            $aNotAllowedVars = $aCliRunConfig['configImportNotAllowedVars'] ?? [];

            $sEnvironment = $this->getOptionEnvironment();
            $sConfigTransferFile = $this->getConfigurationTransferFile(
                $iShopId,
                $sEnvironment
            );

            $aConfigValues = $this->getShopConfiguration($aNotAllowedVars, $iShopId, $sConfigTransferFile);

            // Module-Options
            if (isset($aConfigValues['modules'])) {
                $this->setModuleConfiguration($aConfigValues['modules'], $iShopId);
            }

            // Theme-Options
            if (isset($aConfigValues['themes'])) {
                $this->setThemeConfiguration($aConfigValues['themes'], $iShopId);
            }

            // ShopConfig-Options
            if (isset($aConfigValues['shopconfig'])) {
                $this->setShopConfiguration($aConfigValues['shopconfig'], $iShopId);
            }

            // ShopTableConfig-Options
            if (isset($aConfigValues['shoptable'])) {
                $this->setShopTableConfiguration($aConfigValues['shoptable'], $iShopId);
            }

            // Apply Configuration
            $arguments = [
                'command' => 'oe:module:apply-configuration',
            ];

            // shop-id-argument only for mulitshops necessary
            if ($this->isMultiShop()) {
                $arguments['--shop-id'] = $iShopId;
            }

            $commandInput = new ArrayInput($arguments);
            $app->find('oe:module:apply-configuration')->run($commandInput, $this->output);

            $this->output->writeln(sprintf(
                '<comment>Config imported: `%s`</comment>',
                realpath($sConfigTransferFile)
            ));
        }

        // Cache Clear
        $arguments = [
            'command' => 'trw:clear:cache',
        ];
        $commandInput = new ArrayInput($arguments);
        $app->find('trw:clear:cache')->run($commandInput, $this->output);

        // TplBlock Clear
        $arguments = [
            'command' => 'trw:clear:tplblock',
        ];
        $commandInput = new ArrayInput($arguments);
        $app->find('trw:clear:tplblock')->run($commandInput, $this->output);

        // Update Views
        $arguments = [
            'command' => 'trw:update:views',
        ];
        $commandInput = new ArrayInput($arguments);
        $app->find('trw:update:views')->run($commandInput, $this->output);

        return 0;
    }

    /**
     * set Module Configurations.
     *
     * @param array    $aModuleConfigs ThemeConfigs
     * @param null|int $iShopId        iShopId
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function setModuleConfiguration(array $aModuleConfigs = [], ?int $iShopId = null): void
    {
        $sExportYamlFile = $this->getYamlEnvironmentFile($iShopId);

        $aModuleConfigs = $this->fixModuleConfigs($aModuleConfigs);

        $aExportYaml['modules'] = $aModuleConfigs;

        $this->setYamlConfig($sExportYamlFile, $aExportYaml);

        $oShopConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(
            ShopConfigurationDaoBridgeInterface::class
        );
        $oShopConfiguration = $oShopConfigurationDaoBridge->get();
        $oShopConfigurationDaoBridge->save($oShopConfiguration);

        $this->output->writeln(sprintf(
            '<info>Module-Configurations imported for Shop-ID `%s`</info>',
            $iShopId
        ));
    }

    /**
     * set Theme Configurations.
     *
     * @param array    $aThemeConfigs ThemeConfigs
     * @param null|int $iShopId       iShopId
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws DatabaseConnectionException
     */
    protected function setThemeConfiguration(array $aThemeConfigs = [], ?int $iShopId = null): void
    {
        foreach ($aThemeConfigs as $sThemeId => $aThemeValues) {
            foreach ($aThemeValues['themeSettings'] as $sThemeVar => $aThemeValue) {
                ToolsConfig::updateDBConfig(
                    'theme:' . $sThemeId,
                    $sThemeVar,
                    $aThemeValue['group'],
                    $aThemeValue['type'],
                    $aThemeValue['constraints'],
                    $aThemeValue['value'],
                    $iShopId
                );
            }
            $this->output->writeln(sprintf(
                '<info>Theme-Configurations imported: `%s` for Shop-ID `%s`</info>',
                $sThemeId,
                $iShopId
            ));
        }
    }

    /**
     * set Shop Configurations.
     *
     * @param array    $aShopConfigs ShopConfigs
     * @param null|int $iShopId      iShopId
     *
     * @throws ContainerExceptionInterface
     * @throws DatabaseConnectionException
     * @throws NotFoundExceptionInterface
     */
    protected function setShopConfiguration(array $aShopConfigs = [], ?int $iShopId = null): void
    {
        foreach ($aShopConfigs as $sConfigVar => $aConfigValue) {
            ToolsConfig::updateDBConfig(
                '',
                $sConfigVar,
                $aConfigValue['group'],
                $aConfigValue['type'],
                $aConfigValue['constraints'],
                $aConfigValue['value'],
                $iShopId
            );
        }
        $this->output->writeln(sprintf(
            '<info>Shop-Configurations imported for Shop-ID `%s`</info>',
            $iShopId
        ));
    }

    /**
     * set ShopTable Configurations.
     *
     * @param array    $aShopTableConfigs ShopConfigs
     * @param null|int $iShopId           iShopId
     *
     * @throws Exception
     */
    protected function setShopTableConfiguration(array $aShopTableConfigs = [], ?int $iShopId = null): void
    {
        $oShop = oxNew(Shop::class);
        $oShop->load($iShopId);

        $aParams = [];
        $aParamsLang = [];
        foreach ($aShopTableConfigs as $sConfigVar => $aConfigValue) {
            if (!is_array($aConfigValue['value'])) {
                $aParams[$sConfigVar] = $aConfigValue['value'];
            } else {
                $aParamsLang[$sConfigVar] = $aConfigValue['value'];
            }
        }

        if (count($aParams)) {
            $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxshops');
            $oShop->assign($aParams);
            $oShop->save();
        }

        $aShopLangs = Registry::getLang()->getLanguageIds();

        $aParamsByLangId = [];
        if (count($aParamsLang)) {
            foreach ($aParamsLang as $sConfigVar => $aParamsByLang) {
                foreach ($aParamsByLang as $sLang => $sValue) {
                    $iLang = array_search($sLang, $aShopLangs, true);
                    $aParamsByLangId[$iLang][$sConfigVar] = $sValue;
                }
            }
        }

        foreach ($aParamsByLangId as $iLang => $aParams) {
            $oShop->loadInLang($iLang, $iShopId);
            $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxshops');
            $oShop->assign($aParams);
            $oShop->save();
        }
        $this->output->writeln(sprintf(
            '<info>ShopTable-Configurations imported for Shop-ID `%s`</info>',
            $iShopId
        ));
    }

    /**
     * fix Missing empty Parameters in ModuleConfig.
     *
     * @param array $aModuleConfigs aModuleConfigs
     */
    protected function fixModuleConfigs(array $aModuleConfigs = []): array
    {
        foreach ($aModuleConfigs as $sModuleId => $aModuleValues) {
            if (!isset($aModuleValues['moduleSettings'])) {
                $aModuleConfigs[$sModuleId]['moduleSettings'] = [];
            }
        }

        return $aModuleConfigs;
    }
}
