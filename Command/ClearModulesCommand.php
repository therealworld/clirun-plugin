<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TheRealWorld\CliRunPlugin\Traits\CommandLine;

/**
 * Class ClearModulesCommand.
 */
class ClearModulesCommand extends Command
{
    use CommandLine;

    protected static $defaultName = 'trw:clear:modules';

    /**
     * Configure Command.
     */
    protected function configure(): void
    {
        $this->setDescription('Clean up all module variables from invalid entries');
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $sFunctionCall = 'TheRealWorld\ToolsPlugin\Core\ToolsCache::cleanUpModules';
        if (is_callable($sFunctionCall)) {
            $sFunctionCall();
            $this->output->writeln('<info>Module-Variables cleaned successful.</info>');
        }

        return 0;
    }
}
