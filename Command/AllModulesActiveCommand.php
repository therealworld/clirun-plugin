<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Command;

use Exception;
use OxidEsales\EshopCommunity\Internal\Framework\Module\Setup\Exception\ModuleSetupException;
use OxidEsales\EshopCommunity\Internal\Framework\Module\Setup\Service\ModuleActivationServiceInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TheRealWorld\CliRunPlugin\Traits\CommandLine;
use TheRealWorld\CliRunPlugin\Traits\ShopMaintenance;
use TheRealWorld\CliRunPlugin\Traits\YamlConfig;

/**
 * Class AllModulesActiveCommand.
 */
class AllModulesActiveCommand extends Command
{
    use CommandLine;
    use ShopMaintenance;
    use YamlConfig;

    protected static $defaultName = 'trw:allmodules:active';

    private ModuleActivationServiceInterface $moduleActivationService;

    public function __construct(
        ModuleActivationServiceInterface $moduleActivationService
    ) {
        parent::__construct(null);

        $this->moduleActivationService = $moduleActivationService;
    }

    protected function configure(): void
    {
        $this->setDescription('Set all Modules and the Shop "active"')
            ->addOption(
                '--env',
                '',
                InputOption::VALUE_OPTIONAL,
                'Environment to execute in'
            )->addOption(
                '--shop-id',
                '',
                InputOption::VALUE_OPTIONAL,
                'possible Shop-ID'
            )
        ;
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $aShopIds = $this->getShopIds();

        foreach ($aShopIds as $iShopId) {
            if (
                !$aCliRunConfig = $this->getYamlConfig(
                    $iShopId,
                    $this->getOptionEnvironment()
                )
            ) {
                continue;
            }

            // activate Modules in a certain order
            if (isset($aCliRunConfig['moduleTakeCare'])) {
                $this->activateModules($aCliRunConfig['moduleTakeCare'], $iShopId);
            }

            // set ShopMaintenance Off
            if ($this->setShopMaintenanceOff($iShopId)) {
                $this->output->writeLn(sprintf(
                    '<comment>Maintenance Mode for Shop %s deactivated!</comment>',
                    $iShopId
                ));
            }
        }

        return 0;
    }

    /**
     * disable Active Modules.
     *
     * @param array    $aModuleIds with ModuleIds
     * @param null|int $iShopId    ShopId
     */
    protected function activateModules(array $aModuleIds = [], ?int $iShopId = null): void
    {
        if (is_array($aModuleIds)) {
            foreach ($aModuleIds as $sModuleId) {
                // activate module
                try {
                    $this->moduleActivationService->activate($sModuleId, $iShopId);
                    $this->output->writeLn('<info>' . sprintf(
                        'Module - "%s" for Shop "%s" was activated.',
                        $sModuleId,
                        $iShopId
                    ) . '</info>');
                } catch (ModuleSetupException $exception) {
                    $this->output->writeLn(
                        '<info>' . sprintf(
                            'Module - "%s" for Shop "%s" already active.',
                            $sModuleId,
                            $iShopId
                        ) . '</info>'
                    );
                }
            }
        }
    }
}
