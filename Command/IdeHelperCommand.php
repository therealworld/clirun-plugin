<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Command;

use OxidEsales\EshopCommunity\Internal\Container\ContainerFactory;
use OxidEsales\EshopCommunity\Internal\Framework\Module\Configuration\Dao\ShopConfigurationDaoInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TheRealWorld\CliRunPlugin\Traits\CommandLine;
use Throwable;

/**
 * Class IdeHelperCommand.
 */
class IdeHelperCommand extends Command
{
    use CommandLine;

    protected ?int $shopId = null;

    protected static $defaultName = 'trw:ide:helper';

    public function __construct(
    ) {
        parent::__construct(null);
    }

    /**
     * Configure Command.
     */
    protected function configure(): void
    {
        $this->setDescription('Create IDE-Helper file');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;
        $shopId = (int) $this->getOptionShopId();
        $chain = $this->getChain($shopId);
        $extensions = [];
        foreach ($chain as $parent => $items) {
            foreach ($items as $item) {
                $extension = $this->getExtension($parent, $item);
                if ($extension) {
                    $extensions[] = $extension;
                }
            }
        }
        $filePath = $this->getRealPath('', false) . '.ide-helper_modules_' . $shopId . '.php';
        $output->writeln(
            "Writing helper file for <comment>shop-id</comment>" .
            "=<comment>{$shopId}</comment> to <comment>{$filePath}</comment>"
        );
        $makeFile = $this->makeFile($extensions, $filePath, $shopId);
        if ($makeFile) {
            $output->writeln("<info>File has been created</info>");
        }
        if (!$makeFile) {
            $output->writeln('<error>File could not be written</error>');
        }
        return 0;
    }

    protected function getChain(int $shopId): array
    {
        return ContainerFactory::getInstance()
            ->getContainer()
            ->get(ShopConfigurationDaoInterface::class)
            ->get($shopId)
            ->getClassExtensionsChain()
            ->getChain();
    }

    protected function getExtension(string $parent, string $item): ?string
    {
        try {
            if (class_exists($parent)) {
                return "class_alias(" . $parent . "::class, " . $item . "_parent::class);";
            }
        } catch (Throwable) {
            return null;
        }
        return null;
    }

    protected function makeFile(array $paths, string $filePath, int $shopId): bool
    {
        $contents = implode("\n", $paths);
        $result = file_put_contents(
            $filePath,
            "<?php // IDE Helper for shop {$shopId}\ndeclare(strict_types=1);\n{$contents}"
        );
        return false !== $result;
    }
}
