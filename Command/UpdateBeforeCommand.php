<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Command;

use JsonException;
use OxidEsales\Eshop\Core\Module\Module;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\EshopCommunity\Internal\Container\ContainerFactory;
use OxidEsales\EshopCommunity\Internal\Framework\DIContainer\DataObject\DIConfigWrapper;
use OxidEsales\EshopCommunity\Internal\Framework\Module\Configuration\Bridge\ModuleConfigurationDaoBridgeInterface;
use OxidEsales\EshopCommunity\Internal\Transition\Utility\BasicContext;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;
use TheRealWorld\CliRunPlugin\Traits\CommandLine;
use TheRealWorld\CliRunPlugin\Traits\ShopMaintenance;
use TheRealWorld\CliRunPlugin\Traits\YamlConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsYaml;

/**
 * Class UpdateBeforeCommand.
 */
class UpdateBeforeCommand extends Command
{
    use CommandLine;
    use ShopMaintenance;
    use YamlConfig;

    protected static $defaultName = 'trw:update:before';

    protected function configure(): void
    {
        $this->setDescription('Shutdown the Shop before Update')
            ->addOption(
                '--env',
                '',
                InputOption::VALUE_OPTIONAL,
                'Environment to execute in'
            )->addOption(
                '--shop-id',
                '',
                InputOption::VALUE_OPTIONAL,
                'possible Shop-ID'
            )
        ;
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     *
     * @throws ContainerExceptionInterface
     * @throws ExceptionInterface
     * @throws JsonException
     * @throws NotFoundExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $app = $this->getApplication();

        $aShopIds = $this->getShopIds();

        $bUpdatePossible = true;

        // collect Module-Configs for export
        foreach ($aShopIds as $iShopId) {
            $sEnvironment = $this->getOptionEnvironment();
            if (
                !$aCliRunConfig = $this->getYamlConfig(
                    $iShopId,
                    $sEnvironment
                )
            ) {
                continue;
            }

            if (true !== ($mResult = $this->isUpdatePossible($aCliRunConfig, $iShopId))) {
                $this->output->writeLn($mResult);
                $bUpdatePossible = false;

                continue;
            }

            if (isset($aCliRunConfig['exportModuleConfigs'])) {
                $this->setExportModuleConfig(
                    $iShopId,
                    $aCliRunConfig['exportModuleConfigs'],
                    $sEnvironment
                );
            }

            // disable active Modules in a certain order
            $arguments = [
                'command'   => 'trw:allmodules:deactive',
                '--shop-id' => $iShopId,
            ];
            $commandInput = new ArrayInput($arguments);
            $app->find('trw:allmodules:deactive')->run($commandInput, $this->output);
        }

        if ($bUpdatePossible) {
            // Module Deletions
            $aDeleteModules = [];

            foreach ($aShopIds as $iShopId) {
                if (
                    !$aCliRunConfig = $this->getYamlConfig(
                        $iShopId,
                        $this->getOptionEnvironment()
                    )
                ) {
                    continue;
                }

                if (isset($aCliRunConfig['moduleTakeCare'])) {
                    // only deactivated Modules are deleteable
                    $aDeleteModules = array_unique(
                        array_merge(
                            $aDeleteModules,
                            $aCliRunConfig['moduleTakeCare']
                        )
                    );
                }
                if (isset($aCliRunConfig['moduleTakeCareDevelop'])) {
                    // only deactivated Modules are deleteable
                    $aDeleteModules = array_unique(
                        array_merge(
                            $aDeleteModules,
                            $aCliRunConfig['moduleTakeCareDevelop']
                        )
                    );
                }
            }

            if (count($aDeleteModules)) {
                $this->deleteModules($aDeleteModules);
            }

            // Theme Deletions
            $aDeleteThemes = [];

            foreach ($aShopIds as $iShopId) {
                if (
                    !$aCliRunConfig = $this->getYamlConfig(
                        $iShopId,
                        $this->getOptionEnvironment()
                    )
                ) {
                    continue;
                }

                if (isset($aCliRunConfig['themeTakeCare'])) {
                    $aDeleteThemes = array_unique(
                        array_merge(
                            $aDeleteThemes,
                            $aCliRunConfig['themeTakeCare']
                        )
                    );
                }
            }

            if (count($aDeleteThemes)) {
                $this->deleteThemes($aDeleteThemes);
            }

            if (
                isset($aCliRunConfig['prepareShopForUpdate'])
                && $aCliRunConfig['prepareShopForUpdate']
            ) {
                $this->prepareShopForUpdate();
            }

            $this->prepareGeneratedServiceYaml();
        }

        // Cache Clear
        $arguments = [
            'command' => 'trw:clear:cache',
        ];
        $commandInput = new ArrayInput($arguments);
        $app->find('trw:clear:cache')->run($commandInput, $this->output);

        return 0;
    }

    /**
     * delete Modules.
     *
     * @param array $aDeleteModules with ModuleIds
     */
    protected function deleteModules(array $aDeleteModules = []): void
    {
        foreach ($aDeleteModules as $sModuleId) {
            $oModule = oxNew(Module::class);
            if ($oModule->load($sModuleId)) {
                $this->rrmdir($oModule->getModuleFullPath());
                $this->output->writeLn(sprintf(
                    '<comment>Module-Path "%s" found and deleted</comment>',
                    $oModule->getModuleFullPath()
                ));
            }
        }
    }

    /**
     * delete Themes.
     *
     * @param array $aDeleteThemeIds with ThemeIds
     */
    protected function deleteThemes(array $aDeleteThemeIds = []): void
    {
        $sShopDir = Registry::getConfig()->getConfigParam('sShopDir');

        $shopOutPath = $sShopDir . 'out' . DIRECTORY_SEPARATOR;
        $shopViewPath = $sShopDir . 'Application' . DIRECTORY_SEPARATOR . 'views';

        $themeOutPaths = new Finder();
        $themeOutPaths
            ->directories()
            ->in($shopOutPath)
            ->depth('== 0')
        ;

        $themeViewPaths = new Finder();
        $themeViewPaths
            ->directories()
            ->in($shopViewPath)
            ->depth('== 0')
        ;

        foreach ($themeOutPaths as $themeOutPath) {
            if (in_array($themeOutPath->getFileName(), $aDeleteThemeIds, true)) {
                $this->rrmdir($themeOutPath->getPathName());
                $this->output->writeLn(sprintf(
                    '<comment>Theme-Out-Path "%s" found and deleted</comment>',
                    $themeOutPath->getPathName()
                ));
            }
        }

        foreach ($themeViewPaths as $themeViewPath) {
            if (in_array($themeViewPath->getFileName(), $aDeleteThemeIds, true)) {
                $this->rrmdir($themeViewPath->getPathName());
                $this->output->writeLn(sprintf(
                    '<comment>Theme-View-Path "%s" found and deleted</comment>',
                    $themeViewPath->getPathName()
                ));
            }
        }
    }

    /**
     * prepare the shop so that the update would start.
     */
    protected function prepareShopForUpdate(): void
    {
        $base = $this->getRealPath('source' . DIRECTORY_SEPARATOR, false);

        if (
            file_exists($base . 'index.php')
            && file_exists($base . 'offline.html')
            && (
                !file_exists($base . 'index.html')
                || is_writable($base . 'index.html')
            )
        ) {
            copy($base . 'offline.html', $base . 'index.html');
            rename($base . 'index.php', $base . 'index.php_bak');

            $this->output->writeLn(
                '<comment>OXID-Shop prepared for source-update</comment>'
            );
        }
    }

    /**
     * remove default-Section in generated_services.yaml
     * to prevent PHP-Errors during composer install/update
     * The section _defaults would be recreated during install
     */
    protected function prepareGeneratedServiceYaml(): void
    {
        $context = new BasicContext();
        $sYamlFile = $context->getGeneratedServicesFilePath();

        $sYaml = ToolsYaml::getYamlStringFromFile($sYamlFile);
        $aService = ToolsYaml::getYamlContent($sYaml);

        if (isset($aService[DIConfigWrapper::SERVICE_SECTION]['_defaults'])) {
            unset($aService[DIConfigWrapper::SERVICE_SECTION]['_defaults']);

            ToolsYaml::setDataToYamlFile(
                $sYamlFile,
                $aService
            );

            $this->output->writeLn(
                '<comment>Removed default section in generic_services.yaml</comment>'
            );
        }
    }

    /**
     * set Export Module Config.
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function setExportModuleConfig(
        ?int $iShopId = null,
        array $aConfigs = [],
        string $sEnvironment = ''
    ): void {
        $sExportYamlFile = $this->getYamlEnvironmentFile($iShopId, $sEnvironment);
        $aExportYaml = [];

        foreach ($aConfigs as $sCfgModule => $sCfgVarnames) {
            $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()
                ->getContainer()->get(ModuleConfigurationDaoBridgeInterface::class)
            ;
            $oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sCfgModule);

            foreach ($sCfgVarnames as $sCfgVarname) {
                if ($oModuleConfiguration->hasModuleSetting($sCfgVarname)) {
                    $oModuleSetting = $oModuleConfiguration->getModuleSetting($sCfgVarname);
                    $aExportYaml['modules'][$sCfgModule]['moduleSettings'][$sCfgVarname] = [
                        'group'       => $oModuleSetting->getGroupName(),
                        'type'        => $oModuleSetting->getType(),
                        'value'       => $oModuleSetting->getValue(),
                        'constraints' => $oModuleSetting->getConstraints(),
                    ];

                    $this->output->writeLn(sprintf(
                        '<comment>export Module "%s" Config "%s"</comment>',
                        $sCfgModule,
                        $sCfgVarname
                    ));
                }
            }
        }

        $this->setYamlConfig($sExportYamlFile, $aExportYaml);
    }
}
