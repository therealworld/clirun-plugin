<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Command;

use OxidEsales\Eshop\Core\Module\ModuleList;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Theme;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TheRealWorld\CliRunPlugin\Traits\CommandLine;
use TheRealWorld\CliRunPlugin\Traits\YamlConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsYaml;

/**
 * Class GenerateYamlConfigCommand.
 */
class GenerateYamlConfigCommand extends Command
{
    use CommandLine;
    use YamlConfig;

    protected static $defaultName = 'trw:generate:yaml:config';

    /**
     * Configure Command.
     */
    protected function configure(): void
    {
        $this
            ->setDescription(
                'Generate a configuration Yaml File for command "trw:update:before" and "trw:update:after"'
            )
            ->addOption(
                '--env',
                '',
                InputOption::VALUE_OPTIONAL,
                'Environment to execute in'
            )->addOption(
                '--shop-id',
                '',
                InputOption::VALUE_OPTIONAL,
                'possible Shop-ID'
            )
        ;
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $aShopIds = $this->getShopIds();

        foreach ($aShopIds as $iShopId) {
            $sYamlFile = $this->getYamlConfigFile($iShopId, $this->getOptionEnvironment());
            $aYaml = [];
            if ($ymlString = ToolsYaml::getYamlStringFromFile($sYamlFile)) {
                $aYaml = ToolsYaml::getYamlContent($ymlString);
            }

            $aDefaultModules = array_merge(
                $this->getActiveModules(),
                $this->getDisabledModules()
            );

            // Module Deletions
            if (!isset($aYaml['moduleTakeCare'])) {
                $aYaml['moduleTakeCare'] = $aDefaultModules;
            }

            // Module Develop Deletions
            if (!isset($aYaml['moduleTakeCareDevelop'])) {
                $aYaml['moduleTakeCareDevelop'] = [];
            }

            // Theme Deletions
            if (!isset($aYaml['themeTakeCare'])) {
                $aYaml['themeTakeCare'] = $this->getThemes();
            }

            // Filelist "dontRunIfFileExits"
            if (!isset($aYaml['dontRunIfFileExits'])) {
                $aYaml['dontRunIfFileExits'] = ['dummyfile1.txt'];
            }

            // Filelist "runIfFileExits"
            if (!isset($aYaml['runIfFileExits'])) {
                $aYaml['runIfFileExits'] = ['dummyfile2.txt'];
            }

            // Configlist "dontRunIfOptionValue"
            if (!isset($aYaml['dontRunIfOptionValue'])) {
                $aYaml['dontRunIfOptionValue'] = [];
                $aYaml['dontRunIfOptionValue']['bDemoOption'] = true;
            }

            // Configlist "runIfOptionValue"
            if (!isset($aYaml['runIfOptionValue'])) {
                $aYaml['runIfOptionValue'] = [];
                $aYaml['runIfOptionValue']['bDemoOption'] = true;
            }

            // Configlist "runIfEnvironmentVariable"
            if (!isset($aYaml['runIfEnvironmentVariable'])) {
                $aYaml['runIfEnvironmentVariable'] = [];
                $aYaml['runIfEnvironmentVariable']['DEV_ENVIRONMENT'] = 1;
            }

            // Configlist "prepareShopForUpdate"
            if (!isset($aYaml['prepareShopForUpdate'])) {
                $aYaml['prepareShopForUpdate'] = false;
            }

            // Configlist "exportModuleConfigs"
            if (!isset($aYaml['configExportImportPath'])) {
                $aYaml['configExportImportPath'] = 'transfer';
            }

            // Configlist "configExportNotAllowedVars"
            if (!isset($aYaml['configExportNotAllowedVars'])) {
                $aYaml['configExportNotAllowedVars'] = ['bOptionNotAllowed1', 'bOptionNotAllowed2'];
            }

            // Configlist "configExportNotAllowedVars"
            if (!isset($aYaml['configImportNotAllowedVars'])) {
                $aYaml['configImportNotAllowedVars'] = ['bOptionNotAllowed1', 'bOptionNotAllowed2'];
            }

            ToolsYaml::setDataToYamlFile($sYamlFile, $aYaml);

            $this->output->writeln(sprintf(
                '<comment>Configfile saved. Please edit again: `%s`</comment>',
                realpath($sYamlFile)
            ));
        }

        return 0;
    }

    protected function getActiveModules(): array
    {
        $oModuleList = Registry::get(ModuleList::class);

        return array_keys($oModuleList->getActiveModuleInfo());
    }

    protected function getDisabledModules(): array
    {
        $oModuleList = Registry::get(ModuleList::class);

        return array_keys($oModuleList->getDisabledModuleInfo());
    }

    protected function getThemes(): array
    {
        $themes = [];
        $oThemes = Registry::get(Theme::class);
        $oThemeList = $oThemes->getList();
        foreach ($oThemeList as $oTheme) {
            $themes[] = $oTheme->getInfo('id');
        }

        return $themes;
    }
}
