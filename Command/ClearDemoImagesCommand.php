<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Command;

use OxidEsales\Eshop\Core\Registry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use TheRealWorld\CliRunPlugin\Traits\CommandLine;
use TheRealWorld\CliRunPlugin\Traits\ShopMaintenance;

/**
 * Class ClearDemoImagesCommand.
 */
class ClearDemoImagesCommand extends Command
{
    use CommandLine;
    use ShopMaintenance;

    protected static $defaultName = 'trw:clear:demoimages';

    /**
     * image folders.
     *
     * @var array;
     */
    private array $aImageFolders = [
        'master',
        'generated',
    ];

    /**
     * Configure Command.
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Delete unnecessary demo images from shop installation')
        ;
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $sShopDir = Registry::getConfig()->getConfigParam('sShopDir');

        $sourcePicturePath = $sShopDir .
            DIRECTORY_SEPARATOR . 'out' .
            DIRECTORY_SEPARATOR . 'pictures' .
            DIRECTORY_SEPARATOR;

        $differencePath = DIRECTORY_SEPARATOR . 'vendor' .
            DIRECTORY_SEPARATOR . 'oxid-esales' .
            DIRECTORY_SEPARATOR . 'oxideshop-ce';

        $vendorPicturePath = $sShopDir .
            DIRECTORY_SEPARATOR . '..' . $differencePath .
            DIRECTORY_SEPARATOR . 'source' .
            DIRECTORY_SEPARATOR . 'out' .
            DIRECTORY_SEPARATOR . 'pictures' .
            DIRECTORY_SEPARATOR;

        $aFilesTarget = [];
        foreach ($this->aImageFolders as $sImageFolder) {
            $sOxidImageVendorPath = $vendorPicturePath . $sImageFolder;
            $sOxidImageSourcePath = $sourcePicturePath . $sImageFolder;

            if (
                is_dir($sOxidImageVendorPath)
                && is_dir($sOxidImageSourcePath)
            ) {
                $aFiles = $this->getDirContents($sOxidImageVendorPath);
                foreach ($aFiles as $sFile) {
                    $sFileTarget = str_replace($differencePath, '', $sFile);
                    if (is_file($sFileTarget)) {
                        $aFilesTarget[] = $sFileTarget;
                    }
                }
            }
        }

        if (count($aFilesTarget)) {
            $helper = $this->getHelper('question');

            $bRun = true;
            // check parameter "--no-interaction"
            if ($input->isInteractive()) {
                $question = new ConfirmationQuestion('Should the demo images be deleted? (Y/n) ', true);
                $bRun = $helper->ask($this->input, $this->output, $question);
            }
            if ($bRun) {
                foreach ($aFilesTarget as $sFileTarget) {
                    unlink($sFileTarget);
                }
            }
            $sResult = sprintf('<info>%s Image-Files deleted</info>', count($aFilesTarget));
        } else {
            $sResult = '<info>No unnecessary pictures found for deletion</info>';
        }
        $this->output->writeln($sResult);

        return 0;
    }
}
