<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CliRunPlugin\Command;

use Exception;
use Ifsnop\Mysqldump\Mysqldump;
use OxidEsales\Eshop\Core\Registry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TheRealWorld\CliRunPlugin\Traits\CommandLine;
use TheRealWorld\CliRunPlugin\Traits\PdoMethods;
use TheRealWorld\CliRunPlugin\Traits\YamlConfig;

/**
 * Class DBExportCommand.
 */
class DBExportCommand extends Command
{
    use YamlConfig;
    use CommandLine;
    use PdoMethods;

    protected static $defaultName = 'trw:db:export';

    protected function configure(): void
    {
        $this->setDescription(
            'Export Database to /export folder.
             Optional control via yaml-file in ' . $this->sYamlExportImportPath
        )->addOption(
            '--yaml',
            '',
            InputOption::VALUE_OPTIONAL,
            'Name of yaml-file in in config-folder ' . $this->sYamlExportImportPath
        );
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $cliRunConfig = $this->getYamlExportImportConfig(
            $this->getOptionDBYaml()
        );

        $config = Registry::getConfig();

        $this->export(
            $config->getConfigParam('dbHost'),
            $config->getConfigParam('dbName'),
            $config->getConfigParam('dbUser'),
            $config->getConfigParam('dbPwd'),
            $this->getExportPath() . $cliRunConfig[$this->confKeyDump],
            $cliRunConfig[$this->confKeyTable],
            $cliRunConfig[$this->confKeyAnonymize],
            (string) $config->getConfigParam('dbPort')
        );

        return 0;
    }

    protected function getExportPath(): string
    {
        return $this->getRealPath('export' . DIRECTORY_SEPARATOR, true);
    }

    protected function export(
        string $host,
        string $dbName,
        string $userName,
        string $passWd,
        string $dumpFile,
        array $onlyTables = [],
        array $anonymizeTables = [],
        string $port = ''
    ): void {
        try {
            $pdoDsnConnection = $this->getPdoDsnConnection($host, $dbName, $port);

            $dump = new Mysqldump(
                $pdoDsnConnection,
                $userName,
                $passWd,
                [
                    'add-drop-table' => true,
                    'skip-definer'   => true,
                    'skip-triggers'  => true,
                    'include-tables' => $onlyTables,
                ]
            );

            // anonymize if wanted
            if (count($anonymizeTables)) {
                $dump->setTransformTableRowHook(function ($tableName, array $row) use ($anonymizeTables) {
                    foreach ($anonymizeTables as $anonymizeTable => $anonymizeRows) {
                        if ($tableName === $anonymizeTable) {
                            foreach ($anonymizeRows as $anonymizeRow) {
                                $row[$anonymizeRow] = (string) random_int(1000000, 9999999);
                            }
                        }
                    }

                    return $row;
                });
            }

            $dump->start($dumpFile);

            $this->output->writeLn(sprintf(
                '<comment>Dump completed in %s</comment>',
                $dumpFile
            ));
        } catch (Exception $e) {
            $this->output->writeLn(sprintf(
                '<comment>mysqldump-php error: `%s`</comment>',
                $e->getMessage()
            ));
        }
    }
}
