Composer Plugin for extend the "oe-console cli-tools"
======

![vendor-logo the-real-world.de](picture.png)

### description

New *oe-console* commands

* trw:allmodules:active [--env=XXXX] [--shop-id=1]
* trw:allmodules:deactive [--env=XXXX] [--shop-id=1]
* trw:clear:cache
* trw:clear:demoimages
* trw:clear:modules
* trw:clear:tplblock
* trw:config:export [--env=XXXX] [--shop-id=1]
* trw:config:import [--env=XXXX] [--shop-id=1]
* trw:generate:yaml:config [--env=XXXX] [--shop-id=1]
* trw:update:after [--env=XXXX] [--shop-id=1]
* trw:update:before [--env=XXXX] [--shop-id=1]
* trw:update:views
* trw.ide.helper [--shop-id=1]

## All-Module (de)activations

The "All-Modules" Activate Commands set the Shop in Maintenance Mode

# All-Module active (trw:allmodules:active)

* activate all Modules in a certain order
* set Shop Maintenance Mode "off"
* option --env=XXXX: get the Plugin-Config from file trwclirun.XXXX.1.yaml
* option --shop-id=YYYY:
  * get the Plugin-Config from file trwclirun.XXXX.YYYY.yaml or trwclirun.YYYY.yaml
  * activate the Modules in Shop with this ID

# All-Module deactive (trw:allmodules:deactive)

* set Shop Maintenance Mode "on"
* deactivate all Modules
* option --env=XXXX: get the Plugin-Config from file trwclirun.XXXX.1.yaml
* option --shop-id=YYYY:
  * get the Plugin-Config from file trwclirun.XXXX.YYYY.yaml or trwclirun.YYYY.yaml
  * deactivate the Modules in Shop with this ID
  * if not set, the command check if Plugin-Configs exists for all Shop-Ids

## Clear-Commands

# clean Cache (trw:clear:cache)

* Clean OXID cache

# clear demoimages (trw:clear:demoimages)

* Delete the unecessary demoimages installed during shopinstallation

# clear Modules (trw:clear:modules)

* clear broken Modules Entries & delete unecessary Module-Options

# clear Tpl Blocks (trw:clear:tplblock)

* Deletes duplicate Tpl block entries

## config im- and export commands

# config export (trw:config:export)

* export the Modules-, Themes-, Shop- and ShopTableConfigs
* option --env=XXXX: get the Plugin-Config from file trwclirun.XXXX.1.yaml
* option --shop-id=YYYY:
  * get the Plugin-Config from file trwclirun.XXXX.YYYY.yaml or trwclirun.YYYY.yaml
  * export the Config from Shop with this ID
  * if not set, the command check if Plugin-Configs exists for all Shop-Ids

# config export (trw:config:export)

* import the Modules-, Themes-, Shop- and ShopTableConfigs
* option --env=XXXX: get the Plugin-Config from file trwclirun.XXXX.1.yaml
* option --shop-id=YYYY:
  * get the Plugin-Config from file trwclirun.XXXX.YYYY.yaml or trwclirun.YYYY.yaml
  * import the Config from Shop with this ID
  * if not set, the command check if Plugin-Configs exists for all Shop-Ids

## configuration (trw:generate:yaml:config)

* create a configfile via *oe-console*-command: *oe-console trw:generate:yaml:config*
  [Example: 1.yaml.dist](example/1.yaml.dist)
YAML example:
```yaml
moduleTakeCare:
  - demo1
  - demo2

moduleTakeCareDevelop:
  - demo3
  - demo4

themeTakeCare:
  - flow
  - wave
  - azure
  - mobile

exportModuleConfigs:
  dummymodule:
    - aDummyOption

dontRunIfFileExits:
  - dummyfile1.txt

runIfFileExits:
  - dummyfile2.txt

dontRunIfOptionValue:
  bDemoOption: true

runIfOptionValue:
  bDemoOption: true

runIfEnvironmentVariable:
  DEV_ENVIRONMENT: 1

configExportImportPath: transfer

configExportNotAllowedVars:
  - bOptionNotAllowed1
  - bOptionNotAllowed2

configImportNotAllowedVars:
  - bOptionNotAllowed1
  - bOptionNotAllowed2
...
```

* option --env=XXXX: write the Plugin-Config in file trwclirun.XXXX.1.yaml
* option --shop-id=YYYY:
  * write the Plugin-Config in file trwclirun.XXXX.YYYY.yaml or trwclirun.YYYY.yaml
  * if not set, the command write the Plugin-Configs for all Shop-Ids

* moduleTakeCare: (necessary for command *trw:update:before*, *trw:allmodules:active* and *trw:allmodules:deactive*)
  * List of ModuleIds for deletetion and activation, before and after update commands
  * The sorting ensures the order of activation

* moduleTakeCareDevelop: (necessary for command *trw:update:before* and *trw:allmodules:deactive*)
  * List of ModuleIds for deletetion and deactivation, before update commands

* themeTakeCare: (necessary for command *trw:update:before*)
  * List of Themes, that will be deleted before update commands
  * the OXID-Shop-installer will create the themes again

* prepareShopForUpdate: (necessary for command *trw:update:before*)
  * prepare the shop, so that the install-routine from OXID starts during "composer install"
  * please be careful, start it only if the standard OXID routines of "composer install" start after executing the command
  * the OXID-Shop-installer will create the themes again

* exportModuleConfigs: (necessary for command *trw:update:before*)
  * This Module-options will be saved, before update
  * Some Module-Options would be inserted or manipulated via DB.
    This will be lost during Shop Updates. This Options would be inserted again
    after Shop-Update

* dontRunIfFileExits: (necessary for command *trw:update:before* and *trw:update:after*)
  * dont run this commands if file exists
  * its a possibility to stop automatic updates

* runIfFileExits: (necessary for command *trw:update:before* and *trw:update:after*)
  * run this commands only if file exists
  * its a possibility to stop automatic updates

* dontRunIfOptionValue: (necessary for command *trw:update:before* and *trw:update:after*)
  * dont run this commands if an shop-config-value exists and active
  * its a possibility to stop automatic updates

* runIfOptionValue: (necessary for command *trw:update:before* and *trw:update:after*)
  * run this commands only if if an shop-config-value exists and active
  * its a possibility to stop automatic updates

* runIfEnvironmentVariable: (necessary for command *trw:update:before* and *trw:update:after*)
  * run this commands only if if an ENVIRONMENT-VARIABLE exists
  * its a possibility to stop automatic updates

* configExportImportPath: (necessary for command *trw:config:export* and *trw:config:import*)
  * The path in which the config export is saved
  * relative to root/var/configuration

* configExportNotAllowedVars: (necessary for command *trw:config:export* and *trw:config:import*)
  * Dont export this config-vars

* configImportNotAllowedVars: (necessary for command *trw:config:export* and *trw:config:import*)
  * Dont import this config-vars

## shopupdate-commands

The shopupdate-commands are intended to speed up the shop composer update process.
The following commands can be executed before and after the shop update:

# after Update (trw:update:after)

* option --env=XXXX: get the Plugin-Config from file trwclirun.XXXX.1.yaml
* option --shop-id=YYYY:
  * get the Plugin-Config from file trwclirun.XXXX.YYYY.yaml or trwclirun.YYYY.yaml
  * "after updates"-processes in Shop with this ID
  * if not set, the command check if Plugin-Configs exists for all Shop-Ids
* activate all Modules in a certain order (via trw:allmodules:active command)
* delete unecessary Demo Images (via trw:delete:demoimages command)
* clean cache (via trw:clear:cache command)
* correct tplblocks (via trw:clear:tplblock command)

# before the Update (trw:update:before)

* option --env=XXXX: get the Plugin-Config from file trwclirun.XXXX.1.yaml
* option --shop-id=YYYY:
  * get the Plugin-Config from file trwclirun.XXXX.YYYY.yaml or trwclirun.YYYY.yaml
  * "before updates"-processes in Shop with this ID
  * if not set, the command check if Plugin-Configs exists for all Shop-Ids
* check whether certain conditions are met
* deactivate all Modules (via trw:allmodules:deactive command)
* clean broken Modules Options
* delete all old themes

## Views Update (trw:update:views)

* update the Shop views

## Create IDE-Helper file (trw.ide.helper)

* option --shop-id=YYYY:
* creates a helper file for the modules currently installed in the shop so that IDEs such as phpStorm can find the class chains and thus code completion is possible
* it does not matter whether the module is active or not. what is important is whether it is installed for the shop
* the file is stored in the root directory and has the name ".ide-helper_modules_1.php", for example. The 1 stands for the shop ID
* the file(s) are parallel to the ".ide-helper.php" which is created via "vendor/bin/oe-eshop-ide_helper"
* if no shop ID is provided optionally, the file for the standard shop is created with the ID 1